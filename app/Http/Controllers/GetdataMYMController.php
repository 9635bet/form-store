<?php

namespace App\Http\Controllers;

use App\Exports\ExportUsers;
use App\Models\GetdataMYM;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;

class GetdataMYMController extends Controller
{
    public function GetdataStore(Request $request)
    {

        $customMessage = [
            "name.required" => "Please include your name.", //กรุณาระบุชื่อมาด้วย
            "tel.required" => "Please include your tel.", //กรุณาระบุเบอร์โทรศัพท์มาด้วย
            "tel.max" => "Phone number more than 10 digits", //เบอร์โทรศัพท์มากกว่า 10 หลัก
            "tel.min" => "Phone number less than 10 digits", //เบอร์โทรศัพท์น้อยกว่า 10 หลัก
            "tel.unique" => "duplicate phone number", //เบอร์โทรศัพท์ซ้ำ
        ];

        $validator = Validator::make($request->all(), [
            'name' => ['required'],
            'tel' => 'required|unique:getdata_m_y_m_s|max:10|min:10',
        ], $customMessage);

        if ($validator->fails()) {
            $errors = $validator->errors();
            Alert::error($errors->first());

            return redirect()->back()->with(
                [
                    'status' => false,
                    'data' => GetdataMYM::all(),
                ], 400
            );
        }
        $post = new GetdataMYM();
        $post->name = $request->name;
        $post->tel = $request->tel;
        $post->save();
        $data = GetdataMYM::all();

        Alert::success('save data successfully', 'အကောင့်ရရှိပါပြီရှင့်။');
        return redirect('/')->with(compact('data'));
    }

    public function Showdata()
    {
        $data = GetdataMYM::all();
        return view('showdata')->with(compact('data'));
    }

    public function exportUsersData()
    {

        $fileName = 'users_'. Carbon::now() .'.xlsx';

        return Excel::download(new ExportUsers, $fileName);

    }

}

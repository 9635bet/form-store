<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB as FacadesDB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExportUsers implements FromCollection, WithHeadings
{

    public function headings(): array
    {

        return [

            "ID",

            "Name",

            "Phone No.",

        ];

    }

    public function collection()
    {

        $usersData = FacadesDB::table('getdata_m_y_m_s')->select('id', 'name', 'tel')->get();
        return collect($usersData);

    }

}

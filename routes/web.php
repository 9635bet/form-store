<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/welcome', function () {
    return view('welcome');
})->name('welcome');

Route::get('/', function () {
    return view('index');
})->name('index');

Route::get('/admin/showdata', [App\Http\Controllers\GetdataMYMController::class, 'Showdata'])->name('Showdata');
Route::post('/GetdataStore', [App\Http\Controllers\GetdataMYMController::class, 'GetdataStore'])->name('GetdataStore');
Route::get('/export-users-data', [App\Http\Controllers\GetdataMYMController::class, 'exportUsersData'])->name('exportUsersData');
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>GetData</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.5/flowbite.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.5/flowbite.min.js"></script>
</head>
<style>
    body {
        background: black url({{ asset('images/001.jpg') }}) no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }

    @font-face {
        font-family: Prompt;
        src: url(/font/Prompt-Regular.ttf);
    }

    body {
        font-family: "Prompt";
    }
    .box-center {
        margin-top: 10%;
        background: rgba(255, 255, 255, 0.13);
        border-radius: 16px;
        box-shadow: 0 4px 30px rgba(0, 0, 0, 0.1);
        backdrop-filter: blur(16.5px);
        -webkit-backdrop-filter: blur(16.5px);
        border: 1px solid rgba(255, 255, 255, 0.21);
    }

    .btn-custom{
        width: 100%;
        text-align: center;
        padding: 20px;
        background: hsla(0,0%,100%,.2);
        border: 2px solid hsla(0,0%,100%,.1);
        border-radius: 10px;
        margin-top: 24px;
        cursor: pointer;
        transition: background-color .15s ease;
        font-weight: 400;
        font-family: "Inter",sans-serif;
        text-transform: uppercase;
    }
    .btn-custom:hover {
        background-color: hsla(0, 0%, 100%, .4);
    }
</style>

<body>
@include('sweetalert::alert')
<div class="max-w-7xl mx-auto">
    <div class="flex flex-col items-center justify-center px-6 py-8 mx-auto my-20 lg:py-0 mt-10">
        <div
            class="w-full lg:w-1/2 box-center bg-white rounded-lg shadow-lg dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">
            <div class="p-6 space-y-4 md:space-y-6 sm:p-8">
                <h1
                    class="text-xl text-center font-bold leading-tight tracking-tight text-white md:text-2xl">
                    Register
                </h1>
                <form method="POST" class="space-y-4 md:space-y-6" action="{{ route('GetdataStore') }}">
                                @csrf
                                <div>
                                    <label class="block mb-2 text-sm font-medium text-white">နာမည်
                                    </label>
                                    <input type="text" name="name"
                                        class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                        placeholder="somsak kitkaw">
                                </div>
                                <div>
                                    <label
                                        class="block mb-2 text-sm font-medium text-white">တယ်လီဖုန်းနံပါတ်</label>
                                    <input type="text" name="tel"
                                           placeholder=""
                                        class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                </div>
                                <button type="submit"
                                    class="btn-custom text-white">submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
</body>
</html>

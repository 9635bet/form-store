<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>GetData</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.5/flowbite.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.5/flowbite.min.js"></script>
</head>
<style>
    body {
        background: black url(https://images.pexels.com/photos/36717/amazing-animal-beautiful-beautifull.jpg?cs=srgb&dl=pexels-pixabay-36717.jpg&fm=jpg);
    }

    @font-face {
        font-family: Prompt;
        src: url(/font/Prompt-Regular.ttf);
    }

    body {
        font-family: "Prompt";
    }
</style>

<body>
    @include('sweetalert::alert')
    <div class="max-w-7xl mx-auto">

        <button class="text-xl p-3 bg-blue-500 text-white rounded-lg shadow-lg text-center my-7">
            <a href="{{ route('exportUsersData') }}">Download With Excel</a></button>
        <div class="relative overflow-x-auto">
            <table class="w-full lg:w-1/2 text-sm text-left text-gray-500 dark:text-gray-400">
                <thead class="text-xl text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" class="px-6 py-3">
                            နာမည်
                        </th>
                        <th scope="col" class="px-6 py-3">
                            တယ်လီဖုန်းနံပါတ်
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $rows)
                        <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 text-xl">
                            <th scope="row"
                                class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                {{ $rows->name }}
                            </th>
                            <td class="px-6 py-4">
                                {{ $rows->tel }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
            </div>
</body>
</html>
